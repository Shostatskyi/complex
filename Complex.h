#pragma once
#include <iostream>
using namespace std;

template <class T>
class Complex
{
	T _imag;
	T _real;

public:
	Complex():_imag(0), _real(0) {}
	Complex(T imag, T real): _imag(imag), _real(real) {}
	Complex(Complex const &num):_imag(num._imag), _real(num._real) {}

	// What methods should be here?
};